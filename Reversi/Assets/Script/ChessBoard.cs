﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public partial class ChessBoard : MonoBehaviour {
    public static ChessBoard Instance { get; set; }

    public ChessPiece[,] pieces;
    public List<ChessPiece> listPieces;

    int nBlackPieces;

    List<Vector2Int> validMoves;
    public bool UpdatedValidMoves;

    bool previousTurnHasNoMove;

    public GameObject blackPiecePrefab;
    public GameObject whitePiecePrefab;

    public bool isBlackTurn;

    public GameObject gameResult;
    public bool hasGameEnded;

    int _depth;

    Vector3 boardOffset;

    public Vector3 BoardOffset
    {
        get
        {
            return boardOffset;
        }

        set
        {
            boardOffset = value;
        }
    }

    // Use this for initialization
    void Start () {
        Instance = this;
        BoardOffset = new Vector3(4.0f, 0.0f, 4.0f);

        _depth = 4;

        gameResult.SetActive(false);

        NewGame();

        StartFromTranscript();

    }
	
	// Update is called once per frame
	void Update () {
        if (hasGameEnded)
        {
            return;
        }

        UpdateTimer();

        if (!UpdatedValidMoves)
        {
            validMoves = ScanForValidMoves();
            UpdatedValidMoves = true;

            //hint if there's at least a move
            if (validMoves.Count > 0)
            {
                previousTurnHasNoMove = false;
                BoardHint.Instance.HintValidMoves(validMoves);
            }
            else
            {
                //previous turn also has no move so we end this game
                if (previousTurnHasNoMove)
                {
                    CheckVictory();
                }
                //otherwise we pass turn to our opponent
                else
                {
                    previousTurnHasNoMove = true;

                    Banner.Instance.ShowBanner((isBlackTurn ? "Black" : "White") + " player has to pass");

                    ChangeTurn();
                }
            }
        }

        //user clicked
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            GameRecorder.Instance.UndoAMove();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            GameRecorder.Instance.RedoAMove();
        }
        else
        {
            if (isBlackTurn && PlayerType.isBlackComputer || !isBlackTurn && PlayerType.isWhiteComputer)
            {
                AIDoItsJob();
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2Int clickedPos = ClickedPosition();

                    TryAMove(clickedPos);
                }
            }
        }

    }

    public void NewGame()
    {
        pieces = new ChessPiece[8, 8];
        listPieces = new List<ChessPiece>();
        validMoves = new List<Vector2Int>();

        previousTurnHasNoMove = false;
        UpdatedValidMoves = false;

        isBlackTurn = true;

        nBlackPieces = 0;

        //start state
        CreateChessPiece(ChessType.BlackPiece, 3, 3);
        CreateChessPiece(ChessType.BlackPiece, 4, 4);
        CreateChessPiece(ChessType.WhitePiece, 3, 4);
        CreateChessPiece(ChessType.WhitePiece, 4, 3);
    }

    void StartFromTranscript()
    {
        if (PlayerType.transcript.Length != 0)
        {
            if (CheckValidTranscript(PlayerType.transcript))
            {
                GameRecorder.Instance.StartFromTranscript(PlayerType.transcript);
            }
            else
            {
                Banner.Instance.ShowBanner("Transcript Wrong");
            }
        }

        Banner.Instance.ShowBanner("HAVE A GOOD GAME!");
    }

    Vector2Int ClickedPosition()
    {
        if (!Camera.main)
        {
            Debug.Log("There is no camera!");
            return Vector2Int.zero;
        }

        RaycastHit rcHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rcHit, 50.0f, LayerMask.GetMask("Board")))
        {
            return new Vector2Int((int)(rcHit.point.x + BoardOffset.x), (int)(rcHit.point.z + BoardOffset.z));
        }
        else
        {
            return new Vector2Int(-1, -1);
        }
    }

    void CreateChessPiece(ChessType type, int x, int y)
    {
        //already created
        if (pieces[x, y] != null)
        {
            return;
        }

        GameObject obj;
        if (type == ChessType.BlackPiece)
        {
            obj = Instantiate(blackPiecePrefab);
            nBlackPieces++;
        }
        else
        {
            obj = Instantiate(whitePiecePrefab);
        }
        ChessPiece p = obj.GetComponent<ChessPiece>();

        p.transform.parent = transform;

        MoveChessPiece(p, x, y);

        listPieces.Add(p);
        pieces[x, y] = p;
    }

    void CreateChessPiece(ChessType type, Vector2Int pos)
    {
        CreateChessPiece(type, pos.x, pos.y);
    }

    void MoveChessPiece(ChessPiece p, int x, int y)
    {
        p.MoveTo(Vector3.right * x + Vector3.forward * y - BoardOffset);
        p.ChessPosition = new Vector2Int(x, y);
    }

    void UpdateTimer()
    {
        if (GameInfo.Instance.IsTimeUp())
        {
            Victory(!isBlackTurn);
        }
    }

    void TryAMove(Vector2Int _pos)
    {
        if (_pos.x != -1 && _pos.y != -1)
        {
            if (validMoves.FindIndex(v => v.x == _pos.x && v.y == _pos.y) != -1)
            {
                //flip last lastest piece to normal; except 4 initial pieces 
                if (listPieces.Count > 4)
                {
                    listPieces[listPieces.Count - 1].Flip();
                }

                //hide hint
                BoardHint.Instance.HideHint();

                MakeAMove(_pos);

                //flip lastest piece to hightlight it
                listPieces[listPieces.Count - 1].Flip();

                //Add to transcript
                GameRecorder.Instance.AddAMove(_pos);

                GameInfo.Instance.UpdateScore(nBlackPieces, listPieces.Count - nBlackPieces);

                //Check victory after each move
                if (nBlackPieces == 0
                    || nBlackPieces == listPieces.Count)
                {
                    CheckVictory();
                }
            }
        }
    }

    public void MakeAMove(Vector2Int _pos)
    {
        ReplacePiecesOfOpponent(_pos);

        //place new piece
        CreateChessPiece(isBlackTurn ? ChessType.BlackPiece : ChessType.WhitePiece, _pos);

        ChangeTurn();
    }

    public void ReplacePiecesOfOpponent(Vector2Int _posOfNewPiece)
    {
        //Flip all pieces of opponent
        string pieceTag = StringChessType(isBlackTurn);
        int i = 0, _x = 0, _y = 0;

        //(x  y)
        //-1  1   0  1   1  1
        //-1  0  (0  0)  1  0
        //-1 -1   0 -1   1 -1
        for (int y = -1; y <= 1; y++)
        {
            for (int x = -1; x <= 1; x++)
            {
                //except that piece
                if (x == 0 && y == 0)
                {
                    continue;
                }

                i = 1;
                _x = _posOfNewPiece.x + x;
                _y = _posOfNewPiece.y + y;

                //loop until reach bounds or is not piece of opponent
                while (IsInsideBounds(_x, _y)
                    && pieces[_x, _y] != null
                    && !pieces[_x, _y].CompareTag(pieceTag))
                {
                    _x = _posOfNewPiece.x + x * ++i;
                    _y = _posOfNewPiece.y + y * i;
                }

                //if there is at least one piece of opponent and that direction ends with our piece
                if (i > 1 && IsInsideBounds(_x, _y)
                    && pieces[_x, _y] != null
                    && pieces[_x, _y].CompareTag(pieceTag))
                {
                    //flip chess pieces of opponent which are between our pieces
                    while (i > 1)
                    {
                        _x = _posOfNewPiece.x + x * --i;
                        _y = _posOfNewPiece.y + y * i;

                        //destroy oppent's piece on the BOARD
                        Destroy(pieces[_x, _y].gameObject);

                        //remove from list pieces
                        listPieces.Remove(pieces[_x, _y]);
                        pieces[_x, _y] = null;

                        //create our piece
                        if (isBlackTurn)
                        {
                            CreateChessPiece(ChessType.BlackPiece, _x, _y);
                        }
                        else
                        {
                            //replace black piece with white one
                            CreateChessPiece(ChessType.WhitePiece, _x, _y);
                            nBlackPieces--;
                        }
                    }
                }
            }
        }
    }

    void ChangeTurn()
    {
        //other turn;
        isBlackTurn = !isBlackTurn;
        UpdatedValidMoves = false;

        if (GameInfo.Instance != null)
        {
            GameInfo.Instance.IsBlackPlaying(isBlackTurn);
        }
    }

    bool IsInsideBounds(int x, int y)
    {
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            return true;
        }
        return false;
    }

    List<Vector2Int> ScanForValidMoves()
    {
        List<Vector2Int> posibleMoves = new List<Vector2Int>();

        string pieceTag = StringChessType(isBlackTurn);
        int i = 0, _x = 0, _y = 0;

        //select all of our chess pieces and find out can we move any direction from those
        listPieces.FindAll(p => p.CompareTag(pieceTag)).ForEach(p =>
        {
            //(x  y)
            //-1  1   0  1   1  1
            //-1  0  (0  0)  1  0
            //-1 -1   0 -1   1 -1
            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    //except that piece
                    if (x == 0 && y == 0)
                    {
                        continue;
                    }

                    i = 1;
                    _x = p.ChessPosition.x + x;
                    _y = p.ChessPosition.y + y;

                    //loop until reach bounds or is not piece of opponent
                    while (IsInsideBounds(_x, _y)
                        && pieces[_x, _y] != null
                        && !pieces[_x, _y].CompareTag(pieceTag))
                    {
                        _x = p.ChessPosition.x + x * ++i;
                        _y = p.ChessPosition.y + y * i;
                    }

                    //if there is at least one piece of opponent in that position
                    //and that direction ends with a room for our piece
                    if (i > 1
                        && IsInsideBounds(_x, _y)
                        && pieces[_x, _y] == null)
                    {
                        posibleMoves.Add(new Vector2Int(_x, _y));
                    }
                }
            }
        });

        return posibleMoves;
    }

    void CheckVictory()
    {
        if (nBlackPieces > listPieces.Count / 2)
        {
            Victory(true);
        }
        else if (nBlackPieces < listPieces.Count / 2)
        {
            Victory(false);
        }
        else
        {
            Victory(null);
        }
    }

    void Victory(bool? isBlackWin)
    {
        if (hasGameEnded)
        {
            return;
        }

        gameResult.SetActive(true);

        TMPro.TMP_Text winnerText = gameResult.transform.Find("WinnerText").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_Text blackScoreText = gameResult.transform.Find("BlackScoreText").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_Text whiteScoreText = gameResult.transform.Find("WhiteScoreText").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_InputField transcriptText = gameResult.transform.Find("TranscriptInput").GetComponent<TMPro.TMP_InputField>();

        if (isBlackWin == null)
        {
            winnerText.text = "TIE!";
        }
        else if (isBlackWin == true)
        {
            winnerText.text = "BLACK PLAYER WON!";
        }
        else
        {
            winnerText.text = "WHITE PLAYER WON!";
        }

        blackScoreText.text = "Black score: " + nBlackPieces.ToString();
        whiteScoreText.text = "White score: " + (listPieces.Count - nBlackPieces).ToString();

        transcriptText.text = "<color=#F0F0F0>" + GameRecorder.Instance.transcript + "</a>";
        hasGameEnded = true;
    }

    void AIDoItsJob()
    {
        TypePiece[,] _cloneBoard = cloneBoard(pieces);
        string goAway = ScriptAgree(GameRecorder.Instance.transcript);
        if (PlayerType.isBlackComputer)
        {
            if(PlayerType.blackComputerLevel == ComputerLevel.NotEasy && goAway != null)
            {
                bestMove = GameRecorder.ConvertToVector(goAway);
            }
            else
            {
                MinimaxHasPruning(_cloneBoard, _depth, true, Min, Max, TypePiece.Black, PlayerType.blackComputerLevel);
            }
            TryAMove(bestMove);
        }
        if(PlayerType.isWhiteComputer)
        {
            if(PlayerType.whiteComputerLevel == ComputerLevel.NotEasy && ScriptAgree(goAway) != null)
            {
                bestMove = GameRecorder.ConvertToVector(goAway);
            }
            else
            {
                MinimaxHasPruning(_cloneBoard, _depth, true, Min, Max, TypePiece.White, PlayerType.whiteComputerLevel);
            }
            TryAMove(bestMove);
        }
        
    }
    public bool CheckValidTranscript(string _transcript)
    {
        if (_transcript.Length % 2 != 0)
        {
            return false;
        }

        TypePiece[,] myPieces = cloneBoard(pieces);
        TypePiece _typePiece = TypePiece.Black;
        int lengthTranScipt = _transcript.Length;

        for (int i = 0; i < lengthTranScipt; i += 2)
        {
            Vector2Int _pos = GameRecorder.ConvertToVector(_transcript[i].ToString() + _transcript[i + 1].ToString());
            List<Vector2Int> moves = GetPosCanMoves(myPieces, _typePiece);
            if (moves.FindIndex(p => p.x == _pos.x && p.y == _pos.y) != -1)
            {
                MakeAMove(myPieces, _pos, _typePiece);
            }
            else
            {
                return false;
            }
            _typePiece = InvertType(_typePiece);
        }
        return true;
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ResetGame()
    {
        hasGameEnded = false;

        //hide end screen
        gameResult.SetActive(false);

        //reset transcript
        GameRecorder.Instance.ResetGame();

        //reset timer
        GameInfo.Instance.ResetGame();

        GameInfo.Instance.UpdateScore(nBlackPieces, listPieces.Count - nBlackPieces);

        NewGame();
    }
}
