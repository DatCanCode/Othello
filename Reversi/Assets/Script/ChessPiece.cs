﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessPiece : MonoBehaviour {
    private Vector3 chessPieceOffset;
    private Vector2Int chessPosition;

    public Vector2Int ChessPosition
    {
        get
        {
            return chessPosition;
        }

        set
        {
            chessPosition = value;
        }
    }

    private void Awake()
    {
        chessPieceOffset = new Vector3(0.5f, 0.0f, 0.5f);
    }

    public void MoveTo(Vector3 _pos)
    {
        transform.position = _pos + chessPieceOffset;
    }

    public void Flip()
    {
        transform.Rotate(Vector3.right * 180);
    }

}
