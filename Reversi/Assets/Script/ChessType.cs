﻿public partial class ChessBoard
{
    private enum ChessType
    {
        BlackPiece,
        WhitePiece
    }

    private string StringChessType(bool isBlackTurn)
    {
        if (isBlackTurn)
        {
            return "BlackPiece";
        }
        return "WhitePiece";
    }
}
