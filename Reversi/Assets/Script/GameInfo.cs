﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour {
    public static GameInfo Instance { get; set; }

    public float speed;

    public Camera cam;

    public float timeForAGame;

    public TMPro.TMP_Text whiteInfo;
    public TMPro.TMP_Text blackInfo;

    public GameObject whitePiecePrefab;
    public GameObject blackPiecePrefab;

    private GameObject whitePiece;
    private Vector3 whiteInfoOffset;

    private GameObject blackPiece;
    private Vector3 blackInfoOffset;

    private bool isBlackTurn;

    private float whiteTimer;
    private float blackTimer;

    private Color defaultColorOfPieces;
    private Color fadedColorOfPieces;

    private string previousString;

    private string timeFormat;
    

    // Use this for initialization
    void Start () {
        Instance = this;

        whiteInfoOffset = new Vector3(6, 0, 10);
        blackInfoOffset = new Vector3(6, -2, 10);


        //float piece
        whitePiece = Instantiate(whitePiecePrefab, cam.transform);
        whitePiece.transform.localPosition = whiteInfoOffset;
        whitePiece.transform.localRotation = new Quaternion(-180f, 0f, 0f, 0f);
        whitePiece.name = "whiteInfoPiece";
        whitePiece.AddComponent<ShowTimerOnHover>();
        whitePiece.AddComponent<BoxCollider>();
        //set white piece's shader's render mode to fade in order to apply transparent effect
        StandardShaderUtils.ChangeRenderMode(whitePiece.GetComponent<Renderer>().material, StandardShaderUtils.BlendMode.Fade);

        blackPiece = Instantiate(blackPiecePrefab, cam.transform);
        blackPiece.transform.localPosition = blackInfoOffset;
        blackPiece.transform.localRotation = new Quaternion(-180f, 0f, 0f, 0f);
        blackPiece.name = "blackInfoPiece";
        blackPiece.AddComponent<ShowTimerOnHover>();
        blackPiece.AddComponent<BoxCollider>();
        //set white piece's shader's render mode to fade in order to apply transparent effect
        StandardShaderUtils.ChangeRenderMode(blackPiece.GetComponent<Renderer>().material, StandardShaderUtils.BlendMode.Fade);

        whiteInfo.gameObject.SetActive(true);
        blackInfo.gameObject.SetActive(true);

        ResetGame();

        defaultColorOfPieces = blackPiece.GetComponent<Renderer>().material.color;
        fadedColorOfPieces = defaultColorOfPieces;
        defaultColorOfPieces.a = 0.7f;
        fadedColorOfPieces.a = 0.12f;

        timeFormat = "  {0:00}:{1:00}";
    }
	
	// Update is called once per frame
	void Update () {
        UpdatePlayerTurn();
        UpdateTimer();
        UpdateVisualizedTimer();
    }

    public void UpdateScore(int _blackScore, int _whiteScore)
    {
        whiteInfo.text = "x " + _whiteScore.ToString();

        blackInfo.text = "x " + _blackScore.ToString();
    }

    void UpdatePlayerTurn()
    {
        float angle = speed * Time.deltaTime * 360;

        if (isBlackTurn)
        {
            blackPiece.transform.Rotate(angle, 0f, 0f);
        }
        else
        {
            whitePiece.transform.Rotate(angle, 0f, 0f);
        }
    }

    void UpdateTimer()
    {
        if (ChessBoard.Instance.hasGameEnded)
        {
            return;
        }

        if (isBlackTurn)
        {
            if (blackTimer > 0)
            {
                blackTimer -= Time.deltaTime;
            }
        }
        else
        {
            if (whiteTimer > 0)
            {
                whiteTimer -= Time.deltaTime;
            }
        }
    }

    void UpdateVisualizedTimer()
    {
        if (isBlackTurn)
        {
            float t = blackTimer / 60 / timeForAGame;
            blackPiece.GetComponent<Renderer>().material.color = Color.Lerp(fadedColorOfPieces, defaultColorOfPieces, t);
        }
        else
        {
            float t = whiteTimer / 60 / timeForAGame;
            whitePiece.GetComponent<Renderer>().material.color = Color.Lerp(fadedColorOfPieces, defaultColorOfPieces, t);
        }
    }
    
    public void IsBlackPlaying(bool _isBlackPlaying)
    {
        isBlackTurn = _isBlackPlaying;
    }
   
    public void OnMouseOverPiece(bool _isBlack)
    {
        if(_isBlack)
        {
            previousString = previousString ?? blackInfo.text;
            blackInfo.text = string.Format(timeFormat, ((int)blackTimer / 60) % 60, blackTimer % 60);
        }
        else
        {
            previousString = previousString ?? whiteInfo.text;
            whiteInfo.text = string.Format(timeFormat, ((int)whiteTimer / 60) % 60, whiteTimer % 60);
        }
    }

    public void OnMouseExitPiece(bool _isBlack)
    {
        if(_isBlack)
        {
            blackInfo.text = previousString;
        }
        else
        {
            whiteInfo.text = previousString;
        }
        previousString = null;
    }

    public bool IsTimeUp()
    {
        if ((isBlackTurn
             && blackTimer <= 0)
          ||(!isBlackTurn
             && whiteTimer <= 0))
        {
            return true;
        }
        return false;
    }

    public void ResetGame()
    {
        isBlackTurn = true;

        whiteTimer = 60 * timeForAGame;
        blackTimer = whiteTimer;
    }
}
