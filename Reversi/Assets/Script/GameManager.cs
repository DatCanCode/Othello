﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Diagnostics;

public class GameManager : MonoBehaviour {
    
    public static GameManager Instance { get; set; }

    public GameObject Splash;
    public GameObject NewGame;

    public Dropdown goBlackComboBox;
    public Dropdown goWhiteComboBox;

    public GameObject goBlackLevelText;
    public GameObject goWhiteLevelText;

    public Dropdown goBlackLevelComboBox;
    public Dropdown goWhiteLevelComboBox;

    public Toggle goTranscriptToggle;
    public InputField goTranscriptInput;

    List<string> playerType;
    List<string> computerLevel;

    // Use this for initialization
    void Start () {
        Instance = this;

        playerType = new List<string> { "Human", "Computer" };
        computerLevel = new List<string> { "Easy", "Quite Easy", "Less Easy", "Not Easy" };

        Splash.SetActive(true);
        NewGame.SetActive(false);

        PopulateCombobox();
    }

    private void Update()
    {
        if (About.Instance.isOpeningAbout && Input.GetMouseButtonDown(1))
        {
            AboutButton();
        }
    }


    public void NewGameButton()
    {
        Splash.SetActive(false);
        NewGame.SetActive(true);
    }

    public void AboutButton()
    {
        About.Instance.OpenAbout();
        Splash.SetActive(!About.Instance.isOpeningAbout);
        
    }

    public void ExitButton()
    {

        Application.Quit();
    }

    public void BookButton()
    {
        Process.Start(@"F:\Code\Unity\Othello\Reversi\Assets\Script\Recource\Book.txt");
    }


    private void PopulateCombobox()
    {
        goBlackComboBox.AddOptions(playerType);
        goWhiteComboBox.AddOptions(playerType);

        goBlackLevelComboBox.AddOptions(computerLevel);
        goWhiteLevelComboBox.AddOptions(computerLevel);

        goBlackLevelComboBox.value = 1;
        goWhiteLevelComboBox.value = 1;
    }

    public void BlackComboBox()
    {
        goBlackLevelText.SetActive((goBlackComboBox.value == 1 ? true : false));
        goBlackLevelComboBox.gameObject.SetActive((goBlackComboBox.value == 1 ? true : false));
    }

    public void WhiteComboBox()
    {
        goWhiteLevelText.SetActive((goWhiteComboBox.value == 1 ? true : false));
        goWhiteLevelComboBox.gameObject.SetActive((goWhiteComboBox.value == 1 ? true : false));
    }

    public void TranscriptToogle()
    {
        goTranscriptInput.interactable = goTranscriptToggle.isOn;
    }

    public void TranscriptInputValidate(string _transcript)
    {
        if (_transcript.Length == 0)
        {
            return;
        }

        //sample: A1B2
        //Even pos for 1-8
        if (_transcript.Length % 2 == 0)
        {
            
            if (_transcript[_transcript.Length - 1] < '1' || _transcript[_transcript.Length - 1] > '8')
            {
                goTranscriptInput.text = _transcript.ToUpper().Remove(_transcript.Length - 1);
            }
        }
        //Odd pos for 1-8
        else
        {
            if (_transcript[_transcript.Length - 1] >= 'a' && _transcript[_transcript.Length - 1] <= 'h')
            {
                goTranscriptInput.text = goTranscriptInput.text.ToUpper();
            }
            else if (_transcript[_transcript.Length - 1] < 'A' || _transcript[_transcript.Length - 1] > 'H')
            {
                goTranscriptInput.text = _transcript.Remove(_transcript.Length - 1);
            }
        }

    }


    public void StartButton()
    {
        PlayerType.isBlackComputer = goBlackComboBox.value == 1 ? true : false;
        PlayerType.isWhiteComputer = goWhiteComboBox.value == 1 ? true : false;

        PlayerType.blackComputerLevel = (global::ComputerLevel)goBlackLevelComboBox.value;
        PlayerType.whiteComputerLevel = (global::ComputerLevel)goWhiteLevelComboBox.value;

        PlayerType.transcript = goTranscriptInput.text;

        SceneManager.LoadScene("Game");
    }
}
