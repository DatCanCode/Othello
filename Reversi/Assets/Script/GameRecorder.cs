﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRecorder : MonoBehaviour {
    public static GameRecorder Instance { get; set; }

    public string transcript;
    int ply;

    void Awake () {
        Instance = this;

        ResetGame();
    }

    private string ConvertToTranscript(Vector2Int _pos)
    {
        string newMove = "";
        newMove += (char)('A' + _pos.x);
        newMove += (8 - _pos.y);

        return newMove;
    }

    public static Vector2Int ConvertToVector(string _pos)
    {
        return new Vector2Int(_pos[0] - 'A', '8' - _pos[1]);
    }

    private Vector2Int GetPosisionAtAPly(int _ply)
    {
        string _stringPos = transcript[_ply * 2].ToString() + transcript[_ply * 2 + 1].ToString();
        Vector2Int _pos = ConvertToVector(_stringPos);

        return _pos;
    }

    public void AddAMove(Vector2Int _pos)
    {
        if (ply * 2 < transcript.Length)
        {
            transcript = transcript.Remove(ply * 2);
        }

        transcript += ConvertToTranscript(_pos);

        ply++;

        Debug.Log(transcript);
    }

    void NewGame()
    {
        //Destroy all pieces
        if (ChessBoard.Instance != null)
        {
            ChessBoard.Instance.listPieces.ForEach(p => Destroy(p.gameObject));
            ChessBoard.Instance.NewGame();
        }

        //hide hint
        if (BoardHint.Instance != null)
        {
            BoardHint.Instance.HideHint();
        }
    }

    public void UndoAMove()
    {
        NewGame();

        if (ply <= 2)
        {
            ply = 0;
        }
        else
        {
            //back to previous move of this player
            ply -= 2;

            Vector2Int _pos;

            //play from the start to that move
            for (int i = 0; i < ply; i++)
            {
                _pos = GetPosisionAtAPly(i);

                ChessBoard.Instance.MakeAMove(_pos);
            }

            //flip lastest piece
            ChessBoard.Instance.listPieces[ply - 1 + 4].Flip();

            //scan for valid moves
            ChessBoard.Instance.UpdatedValidMoves = false;
        }
    }

    public void RedoAMove()
    {
        if (ply == transcript.Length / 2)
        {
          Banner.Instance.ShowBanner("REACHED NEWEST STATE");

        }
        else
        {
            FlipLastestPiece();

            //get the pos of lastest state and do that move;
            Vector2Int _pos = GetPosisionAtAPly(ply++);
            ChessBoard.Instance.MakeAMove(_pos);

            if (ply * 2 + 1 < transcript.Length)
            {
                _pos = GetPosisionAtAPly(ply++);
                ChessBoard.Instance.MakeAMove(_pos);
            }
            
            //flip lastest piece
            ChessBoard.Instance.listPieces[ply -1 + 4].Flip();

            //scan for valid moves
            ChessBoard.Instance.UpdatedValidMoves = false;
            BoardHint.Instance.HideHint();
        }

    }

    public void StartFromTranscript(string _transcript)
    {
        transcript = _transcript;

        ply = -1;

        while (ply < (transcript.Length / 2) - 1)
        {
                //get the pos of lastest state and do that move;
                Vector2Int _pos = GetPosisionAtAPly(++ply);
                ChessBoard.Instance.MakeAMove(_pos);

                //scan for valid moves
                ChessBoard.Instance.UpdatedValidMoves = false;
        }

        FlipLastestPiece();

        ply++;

    }

    void FlipLastestPiece()
    {
        //flip pre newest chesspice
        if (ChessBoard.Instance.listPieces.Count > 4)
        {
            ChessBoard.Instance.listPieces[ChessBoard.Instance.listPieces.Count - 1].Flip();
        }
    }

    public void ResetGame()
    {
        transcript = "";
        ply = 0;

        NewGame();
    }

    public void CopyTranscriptToClipboard()
    {
        GUIUtility.systemCopyBuffer = transcript;
    }
}
