﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public partial class ChessBoard
{
    const int Min = int.MinValue;
    const int Max = int.MaxValue;
    private Vector2Int bestMove;
    public  enum TypePiece : byte
    {
        NULL = 0,
        Black = 1,
        White = 2
    }
    private List<Vector2Int> GetPosCanMoves(TypePiece[,] myPieces, TypePiece typePiece)
    {
        int temp = 0, _x = 0, _y = 0;
        List<Vector2Int> posibleMoves = new List<Vector2Int>();

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (myPieces[i, j] == TypePiece.NULL || myPieces[i, j] != typePiece)
                    continue;
                for (int y = -1; y <= 1; y++)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        //except that piece
                        if (x == 0 && y == 0)
                        {
                            continue;
                        }

                        temp = 1;
                        _x = i + x;
                        _y = j + y;

                        //loop until reach bounds or is not piece of opponent
                        while (IsInsideBounds(_x, _y)
                            && myPieces[_x, _y] != TypePiece.NULL
                            && myPieces[_x, _y] != typePiece)
                        {
                            _x = i + x * ++temp;
                            _y = j + y * temp;
                        }

                        //if there is at least one piece of opponent in that position
                        //and that direction ends with a room for our piece
                        if (temp > 1
                            && IsInsideBounds(_x, _y)
                            && myPieces[_x, _y] == TypePiece.NULL)
                        {   
                            if(posibleMoves.FindAll(p => (p.x == _x && p.y == _y)).Count == 0)
                            {
                                posibleMoves.Add(new Vector2Int(_x, _y));
                            }
                        }
                    }
                }

            }
        }
        return posibleMoves;
    }
    public TypePiece[,] cloneBoard(ChessPiece[,] myPieces)
    {
        TypePiece[,] clonePieces = new TypePiece[8, 8];

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if(myPieces[i,j] == null)
                    clonePieces[i, j] = TypePiece.NULL;
                else if (myPieces[i, j].CompareTag("BlackPiece"))
                    clonePieces[i, j] = TypePiece.Black;
                else
                    clonePieces[i, j] = TypePiece.White;
            }
        }
        return clonePieces;
    }
    public TypePiece[,] cloneBoard(TypePiece[,] myPieces)
    {
        TypePiece[,] cloneBoard = new TypePiece[8, 8];
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                cloneBoard[i, j] = myPieces[i, j];
            }
        }
        return cloneBoard;
    }
    public void MakeAMove(TypePiece[,] myPieces, Vector2Int posOfNewPiece, TypePiece typePiece)
    {
        //already created
        if (myPieces[posOfNewPiece.x, posOfNewPiece.y] != TypePiece.NULL)
        {
            return;
        }
        myPieces[posOfNewPiece.x, posOfNewPiece.y] = typePiece;

        int temp = 0, _x = 0, _y = 0;
        for (int y = -1; y <= 1; y++)
        {
            for (int x = -1; x <= 1; x++)
            {
                //except that piece
                if (x == 0 && y == 0)
                {
                    continue;
                }

                temp = 1;
                _x = posOfNewPiece.x + x;
                _y = posOfNewPiece.y + y;

                //loop until reach bounds or is not piece of opponent
                while (IsInsideBounds(_x, _y)
                    && myPieces[_x, _y] != TypePiece.NULL
                    && myPieces[_x, _y] != typePiece)
                {
                    _x = posOfNewPiece.x + x * ++temp;
                    _y = posOfNewPiece.y + y * temp;
                }

                //if there is at least one piece of opponent and that direction ends with our piece
                if (temp > 1 && IsInsideBounds(_x, _y)
                    && myPieces[_x, _y] != TypePiece.NULL
                    && myPieces[_x, _y] == typePiece)
                {
                    //flip chess pieces of opponent which are between our pieces
                    while (temp > 1)
                    {
                        _x = posOfNewPiece.x + x * --temp;
                        _y = posOfNewPiece.y + y * temp;
                        myPieces[_x, _y] = typePiece;
                    }
                }
            }
        }
    }
    public bool EndGame(TypePiece[,] myPieces)
    {
        if (((GetPosCanMoves(myPieces, TypePiece.Black).Count == 0 && GetPosCanMoves(myPieces, TypePiece.White).Count == 0)))
            return true;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (myPieces[i, j] == TypePiece.NULL)
                {
                    return false;
                }
            }
        }
       return true;
    }
    public TypePiece InvertType(TypePiece _typePiece)
    {
        if (_typePiece == TypePiece.White)
            return TypePiece.Black;
        else
            return TypePiece.White;
    }
    public int MinimaxHasPruning(TypePiece[,] myPieces, int depth, bool isMaximizingPlayer, int anpha, int beta, TypePiece _typePiece, ComputerLevel hard)
    {
        if (depth == 0 || EndGame(myPieces))
        {
            if(hard == ComputerLevel.Easy)
            {
                return EvalueteSoEasy(myPieces, _typePiece);
            }
            else if(hard == ComputerLevel.QuiteEasy)
            {
                return EvaluateEasy(myPieces, _typePiece);
            }
            else
            {
                return Evaluate(myPieces, _typePiece);
            }
        }

        if(isMaximizingPlayer)
        {
            int bestValue = Min; 
            List<Vector2Int> moves = GetPosCanMoves(myPieces, _typePiece);
            foreach (var item in moves)
            {
                TypePiece[,] newBoard = cloneBoard(myPieces);
                MakeAMove(newBoard, item, _typePiece);
                int v = MinimaxHasPruning(newBoard, depth - 1, !isMaximizingPlayer, anpha, beta, InvertType(_typePiece),hard);
                if (v >= bestValue)
                {
                    bestValue = v;
                    if(depth == _depth)
                    {
                        bestMove = item;
                    }
                        
                }
                if(bestValue >= anpha)
                {
                    anpha = bestValue;
                }
                if (beta <= anpha)
                    break;
            }
           return bestValue;
        }
        else
        {
            int bestValue = Max;
            List<Vector2Int> moves = GetPosCanMoves(myPieces,_typePiece);
            foreach (var item in moves)
            {
                TypePiece[,] newBoard = cloneBoard(myPieces);
                MakeAMove(newBoard, item, _typePiece);
                int v = MinimaxHasPruning(newBoard, depth - 1, !isMaximizingPlayer, anpha, beta, InvertType(_typePiece),hard);
                if (v <= bestValue)
                {
                    bestValue = v;
                    if (depth == _depth)
                        bestMove = item;
                }
                if (bestValue <= beta)
                {
                    beta = bestValue;
                }
                if (beta <= anpha)
                    break;
            }
            return bestValue;

        }
    }

}