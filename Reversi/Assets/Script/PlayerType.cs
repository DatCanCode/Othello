﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ComputerLevel
{
    Easy,
    QuiteEasy,
    LessEasy,
    NotEasy
}
public class PlayerType : MonoBehaviour {
    static public bool isBlackComputer;
    static public bool isWhiteComputer;

    static public ComputerLevel blackComputerLevel;
    static public ComputerLevel whiteComputerLevel;

    static public string transcript;

    public static ComputerLevel ConvertToComputerLevel(string _pcLevel)
    {
        switch (_pcLevel)
        {
            case "Easy":
                return ComputerLevel.Easy;
            case "Quite Easy":
                return ComputerLevel.QuiteEasy;
            case "Less Easy":
                return ComputerLevel.LessEasy;
            case "Not Easy":
                return ComputerLevel.NotEasy;
            default:
                return ComputerLevel.NotEasy;
        }
    }
}