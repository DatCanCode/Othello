﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTimerOnHover : MonoBehaviour {
    // Use this for initialization
    private void OnMouseOver()
    {
        GameInfo.Instance.OnMouseOverPiece(this.CompareTag("BlackPiece"));
    }

    private void OnMouseExit()
    {
        GameInfo.Instance.OnMouseExitPiece(this.CompareTag("BlackPiece"));
    }
}
